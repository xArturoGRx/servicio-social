<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder{

    public function run(){

        \App\Models\User::create([
            'matricula' => '453466534',
            'nombre' => 'Usuario',
            'apellido_paterno' => 'Sistema',
            'apellido_materno' => 'UPQ',
            'sexo' => 1,
            'email' => 'usuario@upq.edu.mx',
            'password' => bcrypt('9_sistema_9'),
        ]);

        \App\Models\Tipo_Usuario::create([
            'nombre_tipo_usuario' => 'Alumno'
        ]);

        \App\Models\grupo::create([
            'id_tutor' => 1,
            'nombre_grupo' => 'S162'
        ]);

        \App\Models\grupo::create([
            'id_tutor' => 1,
            'nombre_grupo' => 'S174'
        ]);

        \App\Models\Carrera::create([
            'nombre_carrera' => 'Ingeniería en Sistemas Computacionales (ISC)',
            'descripcion_carrera' => 'Carrera Ingeniería en Sistemas Computacionales'
        ]);

        \App\Models\Departamento::create([
            'nombre_departamento' => 'Desarrollo Humano',
            'descripcion_departamento' => 'Departamento de desarrollo humano'
        ]);
    }
}
