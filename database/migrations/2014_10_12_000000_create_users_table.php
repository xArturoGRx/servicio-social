<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_carrera')->nullable();
            $table->foreignId('id_grupo')->nullable();
            $table->foreignId('id_tipo_usuario')->nullable();
            $table->string('matricula', 250)->nullable();
            $table->string('nombre', 250);
            $table->string('apellido_paterno', 250);
            $table->string('apellido_materno', 250);
            $table->integer('sexo')->nullable();
            $table->string('num_telefono', 250)->nullable();
            $table->string('num_telefono2', 250)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('ubicacion')->nullable();
            $table->text('descripcion')->nullable();
            $table->text('foto')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
