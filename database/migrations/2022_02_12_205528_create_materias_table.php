<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materias', function (Blueprint $table) {
            $table->id('id_materia');
            $table->foreignId('id_carrera')->nullable();
            $table->foreignId('id_departamento')->nullable();
            $table->string('nombre_materia');
            $table->integer('cuatrimestre')->nullable();
            $table->integer('cupo')->nullable();
            $table->string('imagen')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('materias');
    }
};
