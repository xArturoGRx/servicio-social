<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios', function (Blueprint $table) {
            $table->id('id_anuncios');
            $table->foreignId('id_carrera');
            $table->foreignId('id_empresa_asociada')->nullable();
            $table->string('nombre_anuncio', 250)->nullable();
            $table->text('descripcion')->nullable();
            $table->string('foto', 250)->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('anuncios');
    }
};
