<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa__asociadas', function (Blueprint $table) {
            $table->id('id_empresa');
            $table->string('nombre_empresa', 250);
            $table->text('direccion')->nullable();
            $table->string('foto', 250)->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('empresa__asociadas');
    }
};
