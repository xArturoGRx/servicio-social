<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alta__bajas', function (Blueprint $table) {
            $table->id('id_solicitud');
            $table->foreignId('id_carrera')->nullable();
            $table->foreignId('id_usuario')->nullable();
            $table->foreignId('id_materia')->nullable();
            $table->string('tipo_proceso', 250);
            $table->boolean('aprobacion_tutor')->nullable();
            $table->text('nota_tutor')->nullable();
            $table->boolean('aprobacion_coordinador')->nullable();
            $table->text('nota_coordinador')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('alta__bajas');
    }
};
