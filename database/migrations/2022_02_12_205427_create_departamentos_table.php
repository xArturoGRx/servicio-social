<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->id('id_departamento');
            $table->string('nombre_departamento', 250);
            $table->text('descripcion_departamento')->nullable();
            $table->string('imagen')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('departamentos');
    }
};
