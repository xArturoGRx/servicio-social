<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacantes', function (Blueprint $table) {
            $table->id('id_vacante');
            $table->foreignId('id_carrera');
            $table->string('nombre_vacante', 250)->nullable();
            $table->text('descripcion_vacante')->nullable();
            $table->string('imagen', 250)->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('vacantes');
    }
};
