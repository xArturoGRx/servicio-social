<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materia__asignadas', function (Blueprint $table) {
            $table->id('id_materia_asignada');
            $table->foreignId('id_grupo');
            $table->foreignId('id_materia');
            $table->foreignId('id_usuario');
            $table->boolean('lunes')->nullable();
            $table->time('lunes_hora_inicio')->nullable();
            $table->time('lunes_hora_fin')->nullable();
            $table->boolean('martes')->nullable();
            $table->time('martes_hora_inicio')->nullable();
            $table->time('martes_hora_fin')->nullable();
            $table->boolean('miercoles')->nullable();
            $table->time('miercoles_hora_inicio')->nullable();
            $table->time('miercoles_hora_fin')->nullable();
            $table->boolean('jueves')->nullable();
            $table->time('jueves_hora_inicio')->nullable();
            $table->time('jueves_hora_fin')->nullable();
            $table->boolean('viernes')->nullable();
            $table->time('viernes_hora_inicio')->nullable();
            $table->time('viernes_hora_fin')->nullable();
            $table->boolean('sabado')->nullable();
            $table->time('sabado_hora_inicio')->nullable();
            $table->time('sabado_hora_fin')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('materia__asignadas');
    }
};
