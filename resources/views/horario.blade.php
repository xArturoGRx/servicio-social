@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="row justify-content-center mt-50 pt-5">Horarios de Alumnos</h1>
	<div class="container-fluid">
	
	</div>
	
    <div class="row justify-content-center mt-5 pt-5">
		
	<p align="right">
	</p>
		
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
				<table class="table">
  				<thead class="table-light">
   				 <tr>
      			<th scope="col">Carrera</th>
     			<th scope="col">Grupo</th>
      			<th scope="col">Tutor</th>
     			<th scope="col">Cuatrimestre</th>
    	</tr>
  		</thead>
  		<tbody>
    			<tr>
      			<td>Nombre alumno de ejemplo</td>
      			<td>199</td>
      			<td>Nombre del tutor</td>
				<td>10</td>
    			</tr>

    			<tr>
      			<td>Nombre alumno de ejemplo</td>
      			<td>199</td>
      			<td>Nombre del tutor</td>
				<td>10</td>
    			</tr>

    			<tr>
      			<td>Nombre alumno de ejemplo</td>
      			<td>199</td>
      			<td>Nombre del tutor</td>
				<td>10</td>
    			</tr>

				<tr>
      			<td>Nombre alumno de ejemplo</td>
      			<td>199</td>
      			<td>Nombre del tutor</td>
				<td>10</td>
    			</tr>

				<tr>
      			<td>Nombre alumno de ejemplo</td>
      			<td>199</td>
      			<td>Nombre del tutor</td>
				<td>10</td>
    			</tr>
  		</tbody>
		</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection