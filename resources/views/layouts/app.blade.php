<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap_custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark shadow-sm" style="background-color: #151830; width:100%;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/upq.svg') }}" height="32" width="32"> {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Crear usuario') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Alumno
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('materias_asignadas.index') }}">
                                        Materias asignadas
                                    </a>
                                    <a class="dropdown-item" href="{{ route('horario.index') }}">
                                        Horario
                                    </a>
                                    <a class="dropdown-item" href="{{ route('asesoria.index') }}">
                                        Asesorías
                                    </a>
                                    <a class="dropdown-item" href="{{ route('anuncio.index') }}">
                                        Anuncios
                                    </a>
                                    <a class="dropdown-item" href="{{ route('vacante.index') }}">
                                        Vacantes
                                    </a>
                                    <a class="dropdown-item" href="{{ route('maestro.index') }}">
                                        Información de PA y PTC
                                    </a>
                                    <a class="dropdown-item" href="{{ route('altas_bajas.index') }}">
                                        Altas y bajas de materias
                                    </a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Sistema
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('usuario.index') }}">
                                        Usuarios
                                    </a>
                                    <a class="dropdown-item" href="{{ route('tipo_usuario.index') }}">
                                        Tipos de usuarios
                                    </a>
                                    <a class="dropdown-item" href="{{ route('grupo.index') }}">
                                        Grupos
                                    </a>
                                    <a class="dropdown-item" href="{{ route('carrera.index') }}">
                                        Carreras
                                    </a>
                                    <a class="dropdown-item" href="{{ route('departamento.index') }}">
                                        Departamentos
                                    </a>
                                    <a class="dropdown-item" href="{{ route('materia.index') }}">
                                        Materias
                                    </a>
                                </div>
                            </li>
                            @inject('UsuarioModel', 'App\Models\User')
                            @php
                                $usuario = $UsuarioModel::find(Auth::user()->id);
                            @endphp
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle m-auto" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if($usuario->get_image)
                                        <img src="{{ $usuario->get_image }}" class="rounded-circle" width="25" height="25"> &nbsp;
                                    @else
                                        @if($usuario->sexo == 2)
                                            <img src="{{ asset('img/poli.png') }}" class="rounded-circle" width="25" height="25"> &nbsp;
                                        @else
                                            <img src="{{ asset('img/polo.png') }}" class="rounded-circle" width="25" height="25"> &nbsp;
                                        @endif
                                    @endif
                                    {{ Auth::user()->nombre }} {{ Auth::user()->apellido_paterno }} {{ Auth::user()->apellido_materno }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
