@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5 pt-5">
        <div class="col-lg-4">
            <div class="card mb-4">
                <div class="card-body">
                        <img src="{{ asset('img/upq.svg') }}" class="card-img-top">
                    <h5 class="card-title mt-2">Módulo nombre</h5>
                    <p class="card-text">Descripción del modulo</p>
                    <a href="" class="btn text-white" style="background-color: #45637d">Entrar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
