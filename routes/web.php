<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{AltaBajaController, AnuncioController, AsesoriaController, CarreraController, MaestroController};
use App\Http\Controllers\{DepartamentoController, GrupoController, HorarioController,Maestro,Controller};
use App\Http\Controllers\{MateriaAsignadaController, MateriaController, TipoUsuarioController, UsuarioController, VacanteController};

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/horario', [App\Http\Controllers\HomeController::class, 'horario'])->name('horario');

Route::resource('altas_bajas', AltaBajaController::class)->middleware('auth');
Route::resource('anuncio', AnuncioController::class)->middleware('auth');
Route::resource('asesoria', AsesoriaController::class)->middleware('auth');
Route::resource('carrera', CarreraController::class)->middleware('auth');
Route::resource('departamento', DepartamentoController::class)->middleware('auth');
Route::resource('grupo', GrupoController::class)->middleware('auth');
Route::resource('horario', HorarioController::class)->middleware('auth');
Route::resource('materias_asignadas', MateriaAsignadaController::class)->middleware('auth');
Route::resource('maestro', MaestroController::class)->middleware('auth');
Route::resource('materia', MateriaController::class)->middleware('auth');
Route::resource('tipo_usuario', TipoUsuarioController::class)->middleware('auth');
Route::resource('usuario', UsuarioController::class)->middleware('auth');
Route::resource('vacante', VacanteController::class)->middleware('auth');